var hasCredit = false
val UNIVERSITY_NAME = "Politechnika Krakowska";
val STUDENT_NAME = "Bartłomiej"
val ALBUM_NUMBER = 125695

fun auraColor(studentPoints: Int = 2): Unit {
    val satisfactionLevel = (Math.pow(Math.random(), (110 - studentPoints) / 100.0) * 20).toInt()

    var aura = ""
    when(satisfactionLevel) {
        in 0..5 -> aura = "czerwony"
        in 6..10 -> aura = "pomarańczowy"
        in 11..15 -> aura = "purpurowy"
        in 16..20 -> aura = "zielony"
    }

    printStudentStatus(aura = aura, name = STUDENT_NAME)
}

fun printStudentStatus(name: String, aura: String):Unit {
    println("$name has a $aura face color")
}

fun main() {
    auraColor()
    auraColor(1)
    auraColor(20)
    auraColor(55)
    auraColor(99)
    auraColor(-15)
}
