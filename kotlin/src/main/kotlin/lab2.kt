class Student {
    var name = "bartek"
        get() = field.capitalize()
}

class Student2(var name: String, var university: String = "Ja to na gimnazjum skończyłem edukację, he")

class PrivateStudent(private var name: String, private var university: String) {
    public val showStudent = {-> println("${this.name}, ${this.university}")}
}

open class Person(var name: String)

class DerivedStudent(name: String, var university: String = "Default uni") : Person(name) {
    public val showStudent = {-> println("${this.name}, ${this.university}")}
}

fun main() {

    println("Zadanie 2")
    val studentStatus =
        { studentName: String, studentAura: String -> println("$studentName has a $studentAura face color") }
    studentStatus("Andrzej", "purpura")

    println("\nZadanie 3")
    var student = Student()
    println(student.name)

    println("\nZadanie 4")
    var student2_1 = Student2("Janusz");
    println("${student2_1.name}, ${student2_1.university}")
    var student2_2 = Student2("Buła", "Spóła");
    println("${student2_2.name}, ${student2_2.university}")

    println("\nZadanie 5")
    var studentPrivate = PrivateStudent("Ezio Auditore da Firenze", "Florencja")
    studentPrivate.showStudent()

    println("\nZadanie 6")
    var studentDerived = DerivedStudent("Biały Wilk", "Kaer Morhen")
    studentDerived.showStudent()
}