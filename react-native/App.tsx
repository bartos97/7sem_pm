import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Heading from './app/Heading';

export default function App() {
  return (
      <View style={styles.container}>
          <ScrollView>
            <Heading />
          </ScrollView>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
