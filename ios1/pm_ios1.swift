import Foundation

//task 2 
let points = 5.0
print("Student has \(points) points") 

//task 3
func happiness() -> String  {
    var level = round(pow(Double.random(in:0...1), (110 - points) / 100) * 20)

    let color: String;
    switch level {
        case 0..<5:
            color = "red"
        case 6..<10:
            color = "orange"
        case 11..<15:
            color = "purple"
        default:
            color = "green"
    }
    return color
}
print("Student has \(happiness()) aura")

//task 4 & 5
class Student1 {
    private let firstName: String
    private let lastName: String
    private let points: Double

    init(firstName: String, lastName: String, points: Double) {
        self.firstName = firstName;
        self.lastName = lastName
        self.points = points
    }

    func showStudent() {
        var level = round(pow(Double.random(in:0...1), (110 - points) / 100) * 20)

        let color: String;
        switch level {
            case 0..<5:
                color = "red"
            case 6..<10:
                color = "orange"
            case 11..<15:
                color = "purple"
            default:
                color = "green"
        }

        print("\(firstName) \(lastName) has a \(color) face color") 
    }
}

var s1 = Student1(firstName: "Zbychu", lastName: "Wiadomo", points: 0.0)
s1.showStudent()

//task 6
class Person {
    let firstName: String
    let lastName: String

    init(firstName: String, lastName: String) {
        self.firstName = firstName;
        self.lastName = lastName
    }
}

class Student2 : Person {
    private let points: Double;

    init(firstName: String, lastName: String, points: Double) {
        self.points = points
        super.init(firstName: firstName, lastName: lastName)
    }

    func showStudent() {
        var level = round(pow(Double.random(in:0...1), (110 - points) / 100) * 20)

        let color: String;
        switch level {
            case 0..<5:
                color = "red"
            case 6..<10:
                color = "orange"
            case 11..<15:
                color = "purple"
            default:
                color = "green"
        }

        print("\(firstName) \(lastName) has a \(color) face color") 
    }
}

var s2 = Student2(firstName: "Zbychu", lastName: "Wielonożny", points: 0.7)
s2.showStudent()


