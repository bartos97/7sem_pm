package com.example.lab5

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class FragmentWindow2 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_window2, container, false);
        val textInput = view.findViewById<EditText>(R.id.shared_pref_text_input)
        val saveButton = view.findViewById<Button>(R.id.shared_pref_save_btn)

        val prefKey = getString(R.string.save_value);
        val sharedPref = activity?.getSharedPreferences(prefKey, Context.MODE_PRIVATE)
        val loadedValue: String = sharedPref?.getString(prefKey, "").toString();

        textInput.setText(loadedValue)

        saveButton.setOnClickListener {
            sharedPref?.edit()?.putString(prefKey, textInput.text.toString())?.apply()
            Toast.makeText(activity, "Saved text", Toast.LENGTH_LONG).show();
        }

        return view
    }
}