package com.example.lab5

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonFrag1 = findViewById<Button>(R.id.button_frag1)
        val buttonFrag2 = findViewById<Button>(R.id.button_frag2)

        loadFragment(FragmentWindow1())

        buttonFrag1.setOnClickListener {
            loadFragment(FragmentWindow1())
        }

        buttonFrag2.setOnClickListener {
            loadFragment(FragmentWindow2())
        }
    }

    private fun loadFragment(fragment: androidx.fragment.app.Fragment) {
        val fm: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentsFrame, fragment)
        fragmentTransaction.commit()
    }
}